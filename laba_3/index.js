var http = require('http');
var fs = require('fs');
var multiparty = require('multiparty');

let handler = (req, res) => {
    if(req.url === '/' && req.method === 'GET'){
        fs.readFile('./index.html', (err, data) => {
            if(err){
                res.write('404');
                res.end();
                return;
            }
            res.end(data);
        });
        return;
    }

    if(req.url === '/findfile' && req.method === 'GET'){
        fs.readFile('./findfile.html', (err, data) => {
            if(err){
                res.end('404');
                return;
            }
            res.end(data);
        });    
        return;
    }
    
	if(req.url === '/findfileresult' && req.method === 'POST'){
        let form = new multiparty.Form();
        form.parse(req, (err, fields, files) => {
            if(err){
                res.end('Error parsing form');
                return;
            }
			fs.readdir('./', (err, files) => {
                if(files.includes(String(fields['filename'])))
                    res.end(`File [${String(fields['filename'])}] was found in the directory.`)
                else
                    res.end(`File [${String(fields['filename'])}] was not found.`)
            });        
        })
        return;
    }

	res.end('404');
};

http.createServer(handler).listen(8080);