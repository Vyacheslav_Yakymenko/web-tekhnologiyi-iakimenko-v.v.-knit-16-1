const {Enemy} = require('../models/enemy')

class Terrorist extends Enemy{
    constructor(name, health, damage, isGoodTerrorist){
        super(name, health, damage);
        this.isGoodTerrorist = isGoodTerrorist;
    }
    
    toString(){
        return `Name: ${this.name} --- HP: ${this.health}, DMG: ${this.damage}, GOOD: ${this.isGoodTerrorist}`;
    }
    
    static addTerrorist(terrorist) {
        terroristsList.push(new Terrorist(terrorist.name, terrorist.health, terrorist.damage, terrorist.isGoodTerrorist));
    }
    
    static getAll() {
        return new Promise((resolve, reject) => {
            resolve(terroristsList);
        })
    }
}

const terroristsList = [];
terroristsList.push(new Terrorist('Billy', 13, 5, true));
terroristsList.push(new Terrorist('Willy', 22, 8, true));
terroristsList.push(new Terrorist('Dilly', 18, 10, false));

exports.Terrorist = Terrorist;