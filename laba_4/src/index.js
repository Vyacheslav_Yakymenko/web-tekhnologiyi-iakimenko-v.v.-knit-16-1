var http = require('http');
var bootstrap = require('./bootstrap/bootstrap');
var controllers = require('./controllers');

bootstrap.registerRoute('/', 'GET', controllers.indexController);
bootstrap.registerRoute('/terrorists', 'GET', controllers.terroristListController);
bootstrap.registerRoute('/terrorists/add', 'GET', controllers.terroristFormController);
bootstrap.registerRoute('/terrorists', 'POST', controllers.addTerroristController);
bootstrap.setNotFound(controllers.notFoundController);

http.createServer(bootstrap.bootstrap).listen('8080');
