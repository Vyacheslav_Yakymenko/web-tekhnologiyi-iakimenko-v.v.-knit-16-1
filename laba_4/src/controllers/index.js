var {indexController} = require('./index-controller');
var {notFoundController} = require('./not-found-controller');
var {terroristListController} = require('./terrorist-list-controller');
var {terroristFormController} = require('./terrorist-form-controller');
var {addTerroristController} = require('./add-terrorist-controller');

exports.indexController = indexController;
exports.notFoundController = notFoundController;
exports.terroristListController = terroristListController;
exports.terroristFormController = terroristFormController;
exports.addTerroristController = addTerroristController;