//1.
var fib = []; 
fib[0] = 0, fib[1] = 1;
for (var i = 2; i <= 49; i++) {
  fib[i] = fib[i - 2] + fib[i - 1];
}
fib.forEach(function(n) {
  console.log(n)
});

//2.
function someFunc(num, callback) {
  if(num % 2 != 0)
    callback();
}

someFunc(11, function() {
  console.log('Callback was called. The number is odd.')
})

//3.
function Enemy(name, health, damage){
  this.name = name;
  this.health = health;
  this.damage = damage;
}
function Terrorist(name, health, damage){
  Enemy.call(this, name, health, damage);
  this.shoot = function(enemy){ 
    enemy.health -= this.damage;
  }
}

Terrorist.prototype = Object.create(Enemy.prototype);
Terrorist.prototype.constructor = Terrorist;

var enemy = new Enemy('Enemy', 20, 5);
var terror = new Terrorist('Terrorist', 15, 10);
terror.shoot(enemy);
console.log(enemy.health);