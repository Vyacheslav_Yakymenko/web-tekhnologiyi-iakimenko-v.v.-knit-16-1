var express = require('express');
var router = express.Router();
var controllers = require('../controllers');

router.get('/', controllers.indexController);
router.get('/terrorists', controllers.terroristListController)


router.get('/terrorists/add', controllers.addTerrorist);
router.post('/terrorists', controllers.handleAddController);

router.get('/terrorists/remove/:id', controllers.handleRemoveController);

module.exports.router = router;