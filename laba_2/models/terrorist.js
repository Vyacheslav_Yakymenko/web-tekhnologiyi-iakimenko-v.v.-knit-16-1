const {Enemy} = require('../models/enemy')

exports.Terrorist = class Terrorist extends Enemy{
    constructor(name, health, damage, isGoodTerrorist){
        super(name, health, damage);
        this.isGoodTerrorist = isGoodTerrorist;
    }
}