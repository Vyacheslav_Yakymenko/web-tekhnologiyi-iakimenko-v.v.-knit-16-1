var { createDogs } = require('./services/service')

function checkLength(terrorists){
  return new Promise((resolve, reject) => {
    if (terrorists.length > 2) {
      resolve(terrorists);
    } else {
      reject(new Error('Length error.'));
    }
  });
}

function printTerrorists() {
  createDogs()
  .then(checkLength)
  .then((terrorists) => {
    console.log(terrorists)
  })
  .catch((err) => {
    console.log(err.message);
  })
}

printTerrorists();
