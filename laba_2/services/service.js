var {Terrorist} = require('../models/terrorist');

var condition = true;
exports.createDogs = function createDogs(){
    return new Promise((resolve, reject) => {
        if(condition) {
            var terrorists = [
                new Terrorist('1', 10, 5, true),
                new Terrorist('2', 10, 5, true),
                new Terrorist('3', 10, 5, false),
                new Terrorist('4', 10, 5, false)
            ]
            resolve(terrorists);
        } else {
            reject(new Error('Error.'));
        }
    });
}